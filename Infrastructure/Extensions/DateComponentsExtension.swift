//
//  DateComponentsExtension.swift
//  Ponto
//
//  Created by Marcos Kobuchi on 21/05/18.
//  Copyright © 2018 undercaffeine. All rights reserved.
//

import Foundation

public extension DateComponents {

    public func formatted() -> String? {
        let formatter: DateComponentsFormatter = DateComponentsFormatter()
        formatter.unitsStyle = .positional
        formatter.allowedUnits = [.hour, .minute]
        formatter.zeroFormattingBehavior = [.pad]
        return formatter.string(from: self)
    }

    public func minutes() -> Int {
        let days: Int = self.day ?? 0
        let hours: Int = self.hour ?? 0
        let minutes: Int = self.minute ?? 0
        return (days * 60 + hours) * 60 + minutes
    }

}
