//
//  DateExtension.swift
//  Ponto
//
//  Created by Marcos Kobuchi on 29/01/18.
//  Copyright © 2018 undercaffeine. All rights reserved.
//

import Foundation

extension Date {

    public func description(dateStyle: DateFormatter.Style, timeStyle: DateFormatter.Style) -> String {
        return DateFormatter.localizedString(from: self, dateStyle: dateStyle, timeStyle: timeStyle)
    }

    public var dateComponents: DateComponents {
        return Calendar.current.dateComponents([.day, .hour, .minute, .second], from: self)
    }

    public func date(dateComponents: DateComponents) -> Date {
        return Calendar.current.date(byAdding: dateComponents, to: self)!
    }

}
