//
//  Equatable.swift
//  Ponto
//
//  Created by Marcos Kobuchi on 29/01/18.
//  Copyright © 2018 undercaffeine. All rights reserved.
//

import Foundation

public func - (lhs: DateComponents, rhs: DateComponents) -> DateComponents {
    var result: DateComponents = DateComponents()
    result.day = (lhs.day ?? 0) - (rhs.day ?? 0)
    result.hour = (lhs.hour ?? 0) - (rhs.hour ?? 0)
    result.minute = (lhs.minute ?? 0) - (rhs.minute ?? 0)
    return result
}

public func + (lhs: DateComponents, rhs: DateComponents) -> DateComponents {
    var result: DateComponents = DateComponents()
    result.day = (lhs.day ?? 0) + (rhs.day ?? 0)
    result.hour = (lhs.hour ?? 0) + (rhs.hour ?? 0)
    result.minute = (lhs.minute ?? 0) + (rhs.minute ?? 0)
    return result
}

public func += (lhs: inout DateComponents, rhs: DateComponents) {
    lhs.day = (lhs.day ?? 0) + (rhs.day ?? 0)
    lhs.hour = (lhs.hour ?? 0) + (rhs.hour ?? 0)
    lhs.minute = (lhs.minute ?? 0) + (rhs.minute ?? 0)
}

public func > (lhs: DateComponents, rhs: DateComponents) -> Bool {
    return lhs.minutes() > rhs.minutes()
}
