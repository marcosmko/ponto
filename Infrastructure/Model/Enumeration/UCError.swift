//
//  UCError.swift
//  Ponto
//
//  Created by Marcos Kobuchi on 15/02/18.
//  Copyright © 2018 undercaffeine. All rights reserved.
//

import Foundation

public enum UCError: Error {
    case databaseFailure
    case journeyIntegrity
}
