//
//  RecordType.swift
//  Ponto
//
//  Created by Marcos Kobuchi on 15/02/18.
//  Copyright © 2018 undercaffeine. All rights reserved.
//

import Foundation

@objc
public enum PeriodType: Int16 {
    case interval
    case work
}
