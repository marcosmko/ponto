//
//  Work.swift
//  
//
//  Created by Marcos Kobuchi on 02/05/18.
//
//

import Foundation
import CoreData

public class Period: NSManagedObject {

    @NSManaged public var startDate: Date!
    @NSManaged public var type: PeriodType

    @NSManaged public var journey: Journey?

}
