//
//  Journey.swift
//  
//
//  Created by Marcos Kobuchi on 02/05/18.
//
//

import Foundation
import CoreData

public class Journey: NSManagedObject {

    @NSManaged public var periods: NSOrderedSet!
    public var _periods: [Period] {
        guard let periods: [Period] = self.periods.array as? [Period] else { return [] }
        return periods
    }

    public var interval: DateComponents {
        return self.total(type: .interval)
    }

    public var work: DateComponents {
        return self.total(type: .work)
    }

    private func total(type: PeriodType) -> DateComponents {
        var total: DateComponents = DateComponents()

        for (index, period) in _periods.enumerated() where period.type == type {
            let current: DateComponents = Calendar.current.dateComponents([.day, .hour, .minute, .second], from: period.startDate)

            let components: DateComponents
            if index + 1 < _periods.count {
                components = Calendar.current.dateComponents([.day, .hour, .minute, .second], from: _periods[index+1].startDate)
            } else {
                components = Calendar.current.dateComponents([.day, .hour, .minute, .second], from: Date())
            }

            total += (components - current)
        }

        return total
    }

    @NSManaged public var endDate: Date?

    public var isPaused: Bool {
        if let period = periods.lastObject as? Period {
            return !isEnded && period.type == .interval
        } else {
            // error
            fatalError("inconsistent")
        }
    }

    public var isEnded: Bool {
        return self.endDate != nil
    }

}
