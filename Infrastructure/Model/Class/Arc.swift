//
//  Arc.swift
//  Ponto
//
//  Created by Marcos Kobuchi on 29/01/18.
//  Copyright © 2018 undercaffeine. All rights reserved.
//

import UIKit

public class Arc: NSObject {

    public let angle: CGFloat
    public let color: UIColor

    public init(angle: CGFloat, color: UIColor) {
        self.angle = angle
        self.color = color
    }

}
