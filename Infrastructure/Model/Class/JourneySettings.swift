//
//  JourneyPrediction.swift
//  Ponto
//
//  Created by Marcos Kobuchi on 15/05/18.
//  Copyright © 2018 undercaffeine. All rights reserved.
//

import UIKit

public class JourneySettings: NSObject {

    public let work: DateComponents = {
        let components: DateComponents = DateComponents(hour: 8)
        return components
    }()

    public let interval: DateComponents = {
        let components: DateComponents = DateComponents(hour: 1)
        return components
    }()

    public var total: DateComponents {
        return work + interval
    }

}
