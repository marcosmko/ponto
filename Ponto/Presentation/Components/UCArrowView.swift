//
//  UCArrowView.swift
//  Ponto
//
//  Created by Marcos Kobuchi on 13/05/18.
//  Copyright © 2018 undercaffeine. All rights reserved.
//

import UIKit

open class UCArrowView: UIView {

    public enum Direction: Int {
        case left
        case right
        case up
        case down

        init(rawInt: Int) {
            if 0 <= rawInt || rawInt <= 2 {
                self.init(rawValue: rawInt)!
            } else {
                self = Direction.right
            }
        }
    }

    open var direction: Direction = Direction.right {
        didSet { self.render() }
    }

    @IBInspectable open var _direction: Int {
        get { return direction.rawValue }
        set { self.direction = Direction(rawInt: newValue) }
    }

    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.layer.mask = CAShapeLayer()
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.mask = CAShapeLayer()
    }

    open override func layoutSubviews() {
        super.layoutSubviews()
        self.render()
    }

    private func render() {
        let path: UIBezierPath = UIBezierPath()

        switch self.direction {
        case .right:
            path.move(to: CGPoint.zero)
            path.addLine(to: CGPoint(x: self.bounds.width, y: self.bounds.height/2))
            path.addLine(to: CGPoint(x: 0, y: self.bounds.height))
        case .left:
            path.move(to: CGPoint(x: self.bounds.width, y: 0))
            path.addLine(to: CGPoint(x: self.bounds.width, y: self.bounds.height))
            path.addLine(to: CGPoint(x: 0, y: self.bounds.height/2))
        case .up:
            path.move(to: CGPoint(x: 0, y: self.bounds.height))
            path.addLine(to: CGPoint(x: self.bounds.width, y: self.bounds.height))
            path.addLine(to: CGPoint(x: self.bounds.width/2, y: self.bounds.height/2))
        case .down:
            path.move(to: CGPoint.zero)
            path.addLine(to: CGPoint(x: self.bounds.width, y: 0))
            path.addLine(to: CGPoint(x: self.bounds.width/2, y: self.bounds.height))
        }

        (self.layer.mask as? CAShapeLayer)?.path = path.cgPath
    }

}
