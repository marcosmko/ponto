//
//  UCTriangleView.swift
//  Ponto
//
//  Created by Marcos Kobuchi on 12/05/18.
//  Copyright © 2018 undercaffeine. All rights reserved.
//

import UIKit

open class UCShapeView: UIView {

    public enum Shape: Int {
        case play
        case pause
        case stop

        init(rawInt: Int) {
            if 0 <= rawInt || rawInt <= 2 {
                self.init(rawValue: rawInt)!
            } else {
                self = Shape.play
            }
        }
    }

    public var shape: Shape = Shape.play {
        didSet { self.render() }
    }

    @IBInspectable public var _shape: Int {
        get { return shape.rawValue }
        set { self.shape = Shape(rawInt: newValue) }
    }

    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.layer.mask = CAShapeLayer()
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.mask = CAShapeLayer()
    }

    open override func layoutSubviews() {
        super.layoutSubviews()
        self.render()
    }

    private func render() {
        switch self.shape {
        case .play:
            self.renderPlay()
        case .pause:
            self.renderPause()
        case .stop:
            self.renderStop()
        }
    }

    private func renderPlay() {
        let path: UIBezierPath = UIBezierPath()
        path.move(to: CGPoint.zero)
        path.addLine(to: CGPoint(x: self.bounds.width, y: self.bounds.height/2))
        path.addLine(to: CGPoint(x: 0, y: self.bounds.height))
        (self.layer.mask as? CAShapeLayer)?.path = path.cgPath
    }

    private func renderPause() {
        let path: UIBezierPath = UIBezierPath()

        path.move(to: CGPoint.zero)
        path.addLine(to: CGPoint(x: 10, y: 0))
        path.addLine(to: CGPoint(x: 10, y: self.bounds.height))
        path.addLine(to: CGPoint(x: 0, y: self.bounds.height))
        path.close()

        path.move(to: CGPoint(x: self.bounds.width, y: 0))
        path.addLine(to: CGPoint(x: self.bounds.width, y: self.bounds.height))
        path.addLine(to: CGPoint(x: self.bounds.width-10, y: self.bounds.height))
        path.addLine(to: CGPoint(x: self.bounds.width-10, y: 0))
        path.close()

        (self.layer.mask as? CAShapeLayer)?.path = path.cgPath
    }

    private func renderStop() {
        let path: UIBezierPath = UIBezierPath()
        let lineWidth: CGFloat = 8
        path.move(to: CGPoint(x: lineWidth/2, y: lineWidth/2))
        path.addLine(to: CGPoint(x: self.bounds.width-lineWidth/2, y: self.bounds.height-lineWidth/2))

        path.move(to: CGPoint(x: self.bounds.width-lineWidth/2, y: lineWidth/2))
        path.addLine(to: CGPoint(x: lineWidth/2, y: self.bounds.height-lineWidth/2))

        (self.layer.mask as? CAShapeLayer)?.path = path.cgPath
        (self.layer.mask as? CAShapeLayer)?.lineWidth = lineWidth
        (self.layer.mask as? CAShapeLayer)?.strokeColor = UIColor.blue.cgColor
    }

}
