//
//  UCCircleButton.swift
//  Ponto
//
//  Created by Marcos Kobuchi on 12/05/18.
//  Copyright © 2018 undercaffeine. All rights reserved.
//

import UIKit

public class UCCircleButton: UIButton {

    private let triangleLayer: CAShapeLayer = CAShapeLayer()

    public override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.addSublayer(triangleLayer)
    }

    public func refresh(with angle: CGFloat) {
        let path: UIBezierPath = UIBezierPath()

        let center: CGPoint = CGPoint(x: self.bounds.width/2, y: self.bounds.height/2)
        let radius: CGFloat = (self.bounds.width)/2
        let radiusO: CGFloat = radius * 1.2

        let normalizedAngle: CGFloat = angle - CGFloat.pi/2
        let position: CGPoint = CGPoint(x: center.x + radiusO*cos(normalizedAngle), y: center.y + radiusO*sin(normalizedAngle))

        path.addArc(withCenter: center,
                    radius: radius-1,
                    startAngle: normalizedAngle - 0.15,
                    endAngle: normalizedAngle + 0.15,
                    clockwise: true)
        path.addLine(to: position)

        triangleLayer.fillColor = UIColor.white.cgColor
        triangleLayer.path = path.cgPath
    }

}
