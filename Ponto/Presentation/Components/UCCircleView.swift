//
//  UCCircleView.swift
//  Ponto
//
//  Created by Marcos Kobuchi on 25/01/18.
//  Copyright © 2018 undercaffeine. All rights reserved.
//

import UIKit

@objc
public protocol UCCircleViewDataSource: NSObjectProtocol {
    func circleView(_ circleView: UCCircleView, numberOfArcsInDate date: Date) -> Int
    func circleView(_ circleView: UCCircleView, startAngleForRowAt index: Int) -> CGFloat
    func circleView(_ circleView: UCCircleView, endAngleForRowAt index: Int) -> CGFloat
}

@objc
public protocol UCCircleViewDelegate: NSObjectProtocol {
    @objc
    optional func circleView(_ circleView: UCCircleView, colorForRowAt index: Int) -> UIColor
}

public enum UCCircleViewRowAnimation: Int {
    case none
}

open class UCCircleView: UIView {

    private var date: Date = Date()

    private let backgroundLayer: CAShapeLayer = CAShapeLayer()
    private var layers: [CAShapeLayer] = []

    @IBOutlet weak open var dataSource: UCCircleViewDataSource?
    @IBOutlet weak open var delegate: UCCircleViewDelegate?

    @IBInspectable open var backgroundStrokeColor: UIColor? {
        get { return UIColor(cgColor: self.backgroundLayer.strokeColor) }
        set { self.backgroundLayer.strokeColor = newValue?.cgColor }
    }

    open override func awakeFromNib() {
        super.awakeFromNib()
        self.reloadData()
    }

    override open func layoutSubviews() {
        super.layoutSubviews()
        self.reloadData()
    }

}

extension UCCircleView {

    private func addArc(startAngle: CGFloat, endAngle: CGFloat, path: UIBezierPath) {
        // as we are considering start angle on top, we need to adjust variables
        let startAngle: CGFloat = startAngle - CGFloat.pi/2
        let endAngle: CGFloat = endAngle - CGFloat.pi/2

        // position
        let center: CGPoint = CGPoint(x: self.bounds.width/2, y: self.bounds.height/2)
        let radius: CGFloat = (self.bounds.width)/2

        // path arc
        path.addArc(withCenter: center,
                    radius: radius,
                    startAngle: startAngle,
                    endAngle: endAngle,
                    clockwise: true)
        path.addLine(to: center)
    }

    private func reloadArc(_ index: Int) {
        guard let dataSource: UCCircleViewDataSource = self.dataSource,
            let delegate: UCCircleViewDelegate = self.delegate else { return }

        let startAngle: CGFloat = dataSource.circleView(self, startAngleForRowAt: index)
        let endAngle: CGFloat = dataSource.circleView(self, endAngleForRowAt: index)
        let fillColor: UIColor? = delegate.circleView?(self, colorForRowAt: index)

        let layer: CAShapeLayer = self.layers[index]

        // create new path
        let path: UIBezierPath = UIBezierPath()
        self.addArc(startAngle: startAngle, endAngle: endAngle, path: path)

        // config lines
        layer.path = path.cgPath
        layer.fillColor = fillColor?.cgColor
    }

}

extension UCCircleView {

    open func reloadData() {
        guard let dataSource: UCCircleViewDataSource = self.dataSource else { return }

        let numberOfArcs: Int = dataSource.circleView(self, numberOfArcsInDate: self.date)
        for _ in stride(from: 0, to: numberOfArcs-self.layers.count, by: 1) {
            self.layers.append(CAShapeLayer())
        }

        for counter in stride(from: 0, to: self.layers.count, by: 1) {
            if counter < numberOfArcs {
                self.layer.addSublayer(self.layers[counter])
            } else {
                self.layers[counter].removeFromSuperlayer()
            }
        }

        for index in 0 ..< numberOfArcs {
            self.reloadArc(index)
        }
    }

    open func reloadArcs(at indexes: [Int], with animation: UCCircleViewRowAnimation) {
        guard let numberOfArcs = indexes.max() else { return }

        for _ in stride(from: 0, to: numberOfArcs + 1 - self.layers.count, by: 1) {
            self.layers.append(CAShapeLayer())
        }

        for counter in stride(from: 0, to: self.layers.count, by: 1) {
            if counter < numberOfArcs {
                self.layer.addSublayer(self.layers[counter])
            } else {
                self.layers[counter].removeFromSuperlayer()
            }
        }

        for index in indexes {
            self.reloadArc(index)
        }
    }

}
