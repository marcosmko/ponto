//
//  UCCircleIndicatorView.swift
//  Ponto
//
//  Created by Marcos Kobuchi on 26/05/18.
//  Copyright © 2018 undercaffeine. All rights reserved.
//

import UIKit

public class UCCircleIndicatorView: UIView {

    public var angle: CGFloat = 0 {
        didSet {
            self.refresh()
        }
    }

    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.layer.mask = CAShapeLayer()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.mask = CAShapeLayer()
    }

    private func refresh() {
        guard let layer: CAShapeLayer = self.layer.mask as? CAShapeLayer else {
            return
        }

        let path: UIBezierPath = UIBezierPath()

        let center: CGPoint = CGPoint(x: self.bounds.width/2, y: self.bounds.height/2)
        let radius: CGFloat = (self.bounds.width)/2

        let normalizedAngle: CGFloat = angle - CGFloat.pi/2
        let position: CGPoint = CGPoint(x: center.x + radius*cos(normalizedAngle), y: center.y + radius*sin(normalizedAngle))

        path.move(to: center)
        path.addLine(to: position)

        layer.lineWidth = 1
        layer.strokeColor = UIColor.black.cgColor
        layer.path = path.cgPath
    }

    public override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.mask?.frame = self.bounds
        self.refresh()
    }

}
