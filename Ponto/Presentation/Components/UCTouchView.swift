//
//  TouchableView.swift
//  Ponto
//
//  Created by Marcos Kobuchi on 27/12/17.
//  Copyright © 2017 undercaffeine. All rights reserved.
//

import UIKit

open class UCTouchView: UIView {

    @IBOutlet weak open var touchView: UIView?

    override open func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let view: UIView? = super.hitTest(point, with: event)
        return (view == self) ? self.touchView : view
    }

}
