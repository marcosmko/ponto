//
//  UCGradientView.swift
//  Ponto
//
//  Created by Marcos Kobuchi on 12/05/18.
//  Copyright © 2018 undercaffeine. All rights reserved.
//

import UIKit

open class UCGradientView: UIView {

    private let gradientLayer: CAGradientLayer = CAGradientLayer()

    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.layer.addSublayer(self.gradientLayer)
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.addSublayer(self.gradientLayer)
    }

    @IBInspectable open var firstColor: UIColor? {
        didSet { self.updateColor() }
    }

    @IBInspectable open var secondColor: UIColor? {
        didSet { self.updateColor() }
    }

    open override func layoutSubviews() {
        super.layoutSubviews()
        self.gradientLayer.frame = self.bounds
    }

    private func updateColor() {
        let firstColor: UIColor = self.firstColor ?? UIColor.clear
        let secondColor: UIColor = self.secondColor ?? UIColor.clear
        self.gradientLayer.colors = [firstColor.cgColor, secondColor.cgColor]
    }

}
