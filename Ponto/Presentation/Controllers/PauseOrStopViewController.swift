//
//  PauseOrStopViewController.swift
//  Ponto
//
//  Created by Marcos Kobuchi on 13/05/18.
//  Copyright © 2018 undercaffeine. All rights reserved.
//

import UIKit

public protocol PauseOrStopDelegate: NSObjectProtocol {
    func pause()
    func stop()
}

public class PauseOrStopViewController: UIViewController {

    public weak var delegate: PauseOrStopDelegate?

    @IBAction private func pause(_ sender: Any) {
        self.delegate?.pause()
        self.close(sender)
    }

    @IBAction func stop(_ sender: Any) {
        self.delegate?.stop()
        self.close(sender)
    }

    @IBAction private func close(_ sender: Any) {
        self.dismiss(animated: false)
    }

}
