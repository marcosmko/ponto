//
//  MainViewController.swift
//  Ponto
//
//  Created by Marcos Kobuchi on 29/01/18.
//  Copyright © 2018 undercaffeine. All rights reserved.
//

import UIKit
import Infrastructure
import Services

public class WorkScheduleViewController: UIViewController {

    private let pauseOrStopSegueIdentifier: String = "pauseOrStop"

    @IBOutlet private weak var timeLabel: UILabel!
    @IBOutlet private weak var titleLabel: UILabel!

    @IBOutlet private weak var circleView: UCCircleView!
    @IBOutlet private weak var playButton: UCCircleButton!
    @IBOutlet private weak var shapeView: UCShapeView!
    @IBOutlet private weak var miniTimeLabel: UILabel!

    @IBOutlet private weak var indicatorView: UCCircleIndicatorView!
    @IBOutlet private weak var timeLabelHorizontalLayoutConstraint: NSLayoutConstraint!
    @IBOutlet private weak var timeLabelVerticalLayoutConstraint: NSLayoutConstraint!

    private var journey: Journey?
    private var settings: JourneySettings = JourneySettings()

    private var arcs: [Arc] = []

    public override func viewDidLoad() {
        super.viewDidLoad()

        self.playButton.isEnabled = false
        WorkScheduleServices.fetch { (journey, error) in
            if error == nil, let journey = journey {
                self.journey = journey
                self.shapeView.shape = journey.isPaused ? .play : .pause
            } else {
                self.shapeView.shape = .play
            }
            self.updateTime()
            self.playButton.isEnabled = true
        }

        Timer.scheduledTimer(timeInterval: 1,
                             target: self,
                             selector: #selector(updateTime),
                             userInfo: nil,
                             repeats: true).fire()
    }

}

extension WorkScheduleViewController {

    @IBAction func play(_ sender: UIButton) {
        guard let journey: Journey = journey else {
            // there is no journey, then start playing!
            self.playButton.isEnabled = false
            WorkScheduleServices.start { (journey, error) in
                if error == nil {
                    self.journey = journey
                    self.shapeView.shape = .pause
                } else {
                }
                self.updateTime()
                self.playButton.isEnabled = true
            }
            return
        }

        // there is a journey running
        if journey.isPaused {
            // pause, then resume
            self.playButton.isEnabled = false
            WorkScheduleServices.addPeriod(to: journey, of: .work) { (error) in
                if error == nil {
                    self.shapeView.shape = .pause
                } else {
                }
                self.updateTime()
                self.playButton.isEnabled = true
            }
        } else {
            // give user choice to pause or stop
            self.performSegue(withIdentifier: pauseOrStopSegueIdentifier, sender: self)
        }
    }

}

extension WorkScheduleViewController: UCCircleViewDataSource, UCCircleViewDelegate {

    public func circleView(_ circleView: UCCircleView, numberOfArcsInDate date: Date) -> Int {
        return arcs.count
    }

    public func circleView(_ circleView: UCCircleView, startAngleForRowAt index: Int) -> CGFloat {
        let arc: Arc = arcs[index]
        return arc.angle
    }

    public func circleView(_ circleView: UCCircleView, endAngleForRowAt index: Int) -> CGFloat {
        let arc: Arc = arcs[(index + 1 == arcs.count ? 0 : index + 1)]
        return arc.angle
    }

    public func circleView(_ circleView: UCCircleView, colorForRowAt index: Int) -> UIColor {
        let arc: Arc = arcs[index]
        return arc.color
    }

    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == pauseOrStopSegueIdentifier,
            let vc: PauseOrStopViewController = segue.destination as? PauseOrStopViewController {

            vc.delegate = self
        }
    }

}

extension WorkScheduleViewController {

    private func angle(for date: Date) -> CGFloat {
        let calendar: Calendar = Calendar.current
        let startPosition: Int = calendar.component(.hour, from: date) * 60 + calendar.component(.minute, from: date)
        let startAngle = CGFloat(startPosition) / (24 * 60) * 2 * CGFloat.pi
        return startAngle
    }

    private func processNoJourney(referenceDate: Date) -> [Arc] {
        let endDate: Date = referenceDate.date(dateComponents: settings.total)

        let referenceDateStartAngle: CGFloat = self.angle(for: referenceDate)
        let referenceDateEndAngle: CGFloat = self.angle(for: endDate)

        return [
            Arc(angle: referenceDateStartAngle, color: UCColor.lightWhite),
            Arc(angle: referenceDateEndAngle, color: UCColor.darkWhite)
        ]
    }

    private func processJourney(referenceDate: Date, journey: Journey) -> [Arc] {
        var arcs: [Arc] = []

        var totalInterval: DateComponents = DateComponents()
        var totalWork: DateComponents = DateComponents()

        for (index, period) in journey._periods.enumerated() {
            let startAngle: CGFloat = self.angle(for: period.startDate)
            let color: UCColor

            // check if we already exceed limits
            if period.type == .work {
                color = (totalWork > settings.work) ? UCColor.blue : UCColor.green
            } else {
                color = (totalInterval > settings.interval) ? UCColor.red : UCColor.yellow
            }

            // append arc
            let arc: Arc = Arc(angle: startAngle, color: color)
            arcs.append(arc)

            let startComponents: DateComponents = period.startDate.dateComponents
            let endComponents: DateComponents = (period == journey._periods.last) ?
                referenceDate.dateComponents : journey._periods[index+1].startDate.dateComponents

            if period.type == .interval && settings.interval > totalInterval {
                totalInterval += endComponents - startComponents
                if totalInterval > settings.interval {
                    let components = endComponents - (totalInterval - settings.interval)
                    let angle = self.angle(for: Calendar.current.date(from: components)!)
                    let color: UIColor = UCColor.red
                    let arc: Arc = Arc(angle: angle, color: color)
                    arcs.append(arc)
                }
            } else if period.type == .work && settings.work > totalWork {
                totalWork += endComponents - startComponents
                if totalWork > settings.work {
                    let components = endComponents - (totalWork - settings.work)
                    let angle = self.angle(for: Calendar.current.date(from: components)!)
                    let color: UIColor = UCColor.blue
                    let arc: Arc = Arc(angle: angle, color: color)
                    arcs.append(arc)
                }
            }
        }

        return arcs
    }

    private func process(referenceDate: Date) {
        // arcs for expected journey
        guard let journey: Journey = self.journey else {
            self.arcs = self.processNoJourney(referenceDate: referenceDate)
            return
        }

        // arcs for all periods
        self.arcs = self.processJourney(referenceDate: referenceDate, journey: journey)

        // arc for current time
        let arc: Arc = Arc(angle: self.angle(for: referenceDate), color: UCColor.lightWhite)
        self.arcs.append(arc)

        // arc for remaining time
        let endDate: Date = journey._periods.first!.startDate.date(dateComponents: settings.total)
        if referenceDate < endDate {
            let arc2: Arc = Arc(angle: self.angle(for: endDate), color: UCColor.darkWhite)
            self.arcs.append(arc2)
        } else {
            let arc2: Arc = Arc(angle: self.angle(for: referenceDate), color: UCColor.darkWhite)
            self.arcs.append(arc2)
        }

    }

    private func updateMiniTime(referenceDate: Date) {
        let radiusO: CGFloat = (270+miniTimeLabel.bounds.width*sqrt(2))/2
        let normalizedAngle: CGFloat = angle(for: referenceDate) - CGFloat.pi/2

        self.timeLabelHorizontalLayoutConstraint.constant = radiusO*cos(normalizedAngle)
        self.timeLabelVerticalLayoutConstraint.constant = radiusO*sin(normalizedAngle)

        self.miniTimeLabel.text = referenceDate.description(dateStyle: .none, timeStyle: .short)
    }

    @objc
    private func updateTime() {
        let date: Date = Date()
        self.process(referenceDate: date)

        self.updateMiniTime(referenceDate: date)
        self.playButton.refresh(with: angle(for: date))

        guard let journey: Journey = self.journey else {
            self.titleLabel.text = "Horário de saída previsto para"

            let endDate: Date = Calendar.current.date(byAdding: settings.total, to: date) ?? date
            self.timeLabel.text = endDate.description(dateStyle: .none, timeStyle: .short)
            self.indicatorView.angle = angle(for: date) + CGFloat.pi
            self.circleView.reloadData()

            return
        }

        if let startDate: Date = journey._periods.first?.startDate {
            self.indicatorView.angle = angle(for: startDate) + CGFloat.pi
        }

        if journey.isPaused {
            let total: DateComponents = settings.interval - journey.interval
            self.timeLabel.text = total.formatted()

            if total.minutes() < 0 {
                self.titleLabel.text = "Intervalo extra"
            } else {
                self.titleLabel.text = "Intervalo restante"
            }
        } else {
            let total: DateComponents = journey.work
            self.timeLabel.text = total.formatted()
            self.titleLabel.text = "Horas trabalhadas"
        }
        self.circleView.reloadData()
    }

}

extension WorkScheduleViewController: PauseOrStopDelegate {

    public func pause() {
        guard let journey = journey else { return }
        self.playButton.isEnabled = false
        WorkScheduleServices.addPeriod(to: journey, of: .interval) { (error) in
            if error == nil {
                self.shapeView.shape = .play
            } else {
            }
            self.updateTime()
            self.playButton.isEnabled = true
        }
    }

    public func stop() {
        guard let journey = journey else { return }
        self.playButton.isEnabled = false
        WorkScheduleServices.stop(journey: journey) { (error) in
            if error == nil {
                self.journey = nil
                self.shapeView.shape = .play
            } else {
            }
            self.updateTime()
            self.playButton.isEnabled = true
        }
    }

}
