//
//  UIColorExtension.swift
//  Ponto
//
//  Created by Marcos Kobuchi on 25/01/18.
//  Copyright © 2018 undercaffeine. All rights reserved.
//

import UIKit

extension UIColor {

    public convenience init?(cgColor: CGColor?) {
        guard let cgColor = cgColor else { return nil }
        self.init(cgColor: cgColor)
    }

    public convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")

        self.init(red: CGFloat(red) / 255.0,
                  green: CGFloat(green) / 255.0,
                  blue: CGFloat(blue) / 255.0,
                  alpha: 1.0)
    }

    public convenience init(hex:Int) {
        self.init(red:(hex >> 16) & 0xff, green:(hex >> 8) & 0xff, blue:hex & 0xff)
    }

}
