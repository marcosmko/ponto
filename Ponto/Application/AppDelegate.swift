//
//  AppDelegate.swift
//  Ponto
//
//  Created by Marcos Kobuchi on 21/12/17.
//  Copyright © 2017 undercaffeine. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

}
