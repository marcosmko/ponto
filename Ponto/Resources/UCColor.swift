//
//  UCColor.swift
//  Ponto
//
//  Created by Marcos Kobuchi on 06/01/18.
//  Copyright © 2018 undercaffeine. All rights reserved.
//

import UIKit

open class UCColor: UIColor {

    open class var pink: UCColor {
        return UCColor(hex: 0xF3244D)
    }

    open class var lightWhite: UCColor {
        return UCColor(white: 1, alpha: 0.3)
    }

    open class var darkWhite: UCColor {
        return UCColor(white: 1, alpha: 0.7)
    }

    override open class var red: UCColor {
        return UCColor(hex: 0xD0021B)
    }

    override open class var orange: UCColor {
        return UCColor(hex: 0xFF6256)
    }

    override open class var green: UCColor {
        return UCColor(hex: 0x60E0B2)
    }

    override open class var yellow: UCColor {
        return UCColor(hex: 0xF5C723)
    }

    override open class var gray: UCColor {
        return UCColor(hex: 0x2D2D2D)
    }

    override open class var blue: UCColor {
        return UCColor(hex: 0x4A90E2)
    }

}
