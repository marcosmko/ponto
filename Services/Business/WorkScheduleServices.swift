//
//  WorkScheduleServices.swift
//  Ponto
//
//  Created by Marcos Kobuchi on 06/01/18.
//  Copyright © 2018 undercaffeine. All rights reserved.
//

import Foundation
import Infrastructure
import Persistence

public class WorkScheduleServices {

    public static func fetch(_ completion: ((_ journey: Journey?, _ error: Error?) -> Void)?) {
        let blockForExecutionInBackground: BlockOperation = BlockOperation(block: {
            var journey: Journey?
            var raisedError: Error? = nil

            do {
                journey = try JourneyDAO.last()
            } catch let error {
                raisedError = error
            }

            if let completion = completion {
                let blockForExecutionInMain: BlockOperation = BlockOperation(block: { completion(journey, raisedError) })
                QueueManager.sharedInstance.executeBlock(blockForExecutionInMain, queueType: QueueManager.QueueType.main)
            }
        })

        QueueManager.sharedInstance.executeBlock(blockForExecutionInBackground, queueType: QueueManager.QueueType.serial)
    }

    public static func start(_ completion: ((_ journey: Journey?, _ error: Error?) -> Void)?) {
        let blockForExecutionInBackground: BlockOperation = BlockOperation(block: {
            var journeyToBeReturned: Journey?
            var raisedError: Error? = nil

            do {
                let journey: Journey = Journey()
                try JourneyDAO.create(journey)

                let period: Period = Period()
                period.startDate = Date()
                period.type = .work
                try PeriodDAO.create(period)

                journey.addToPeriods(period)
                try self.checkIntegrity(of: journey)
                try JourneyDAO.update(journey)

                journeyToBeReturned = journey
            } catch let error {
                raisedError = error
            }

            if let completion = completion {
                let blockForExecutionInMain: BlockOperation = BlockOperation(block: { completion(journeyToBeReturned, raisedError) })
                QueueManager.sharedInstance.executeBlock(blockForExecutionInMain, queueType: QueueManager.QueueType.main)
            }
        })

        QueueManager.sharedInstance.executeBlock(blockForExecutionInBackground, queueType: QueueManager.QueueType.serial)
    }

    public static func addPeriod(to journey: Journey, of type: PeriodType, _ completion: ((_ error: Error?) -> Void)?) {
        let blockForExecutionInBackground: BlockOperation = BlockOperation(block: {
            var raisedError: Error? = nil

            do {
                let period: Period = Period()
                period.type = type
                period.startDate = Date()
                try PeriodDAO.create(period)

                journey.addToPeriods(period)
                try self.checkIntegrity(of: journey)
                try JourneyDAO.update(journey)
            } catch let error {
                raisedError = error
            }

            if let completion = completion {
                let blockForExecutionInMain: BlockOperation = BlockOperation(block: { completion(raisedError) })
                QueueManager.sharedInstance.executeBlock(blockForExecutionInMain, queueType: QueueManager.QueueType.main)
            }
        })

        QueueManager.sharedInstance.executeBlock(blockForExecutionInBackground, queueType: QueueManager.QueueType.serial)
    }

    public static func stop(journey: Journey, _ completion: ((_ error: Error?) -> Void)?) {
        let blockForExecutionInBackground: BlockOperation = BlockOperation(block: {
            var raisedError: Error? = nil

            do {
                // stop
                journey.endDate = Date()
                try self.checkIntegrity(of: journey)
                try JourneyDAO.update(journey)
            } catch let error {
                raisedError = error
            }

            if let completion = completion {
                let blockForExecutionInMain: BlockOperation = BlockOperation(block: { completion(raisedError) })
                QueueManager.sharedInstance.executeBlock(blockForExecutionInMain, queueType: QueueManager.QueueType.main)
            }
        })

        QueueManager.sharedInstance.executeBlock(blockForExecutionInBackground, queueType: QueueManager.QueueType.serial)
    }

    private static func checkIntegrity(of journey: Journey) throws {
        // check if is set of period
        guard let periods: [Period] = journey.periods.array as? [Period] else {
            throw UCError.journeyIntegrity
        }

        // check if we have at least one period
        guard let first: Period = periods.first, let last: Period = periods.last else {
            throw UCError.journeyIntegrity
        }

        // check if periods are ordered
        var date: Date = first.startDate
        for period in periods {
            if period.startDate < date {
                throw UCError.journeyIntegrity
            }
            date = period.startDate
        }

        // check if end date is after all periods
        if let endDate: Date = journey.endDate, endDate < last.startDate {
            throw UCError.journeyIntegrity
        }

        // all tests passed!
    }

}
