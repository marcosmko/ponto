//
//  JourneyDAO.swift
//  Ponto
//
//  Created by Marcos Kobuchi on 02/05/18.
//  Copyright © 2018 undercaffeine. All rights reserved.
//

import Foundation
import CoreData
import Infrastructure

public class JourneyDAO {

    public static func create(_ journey: Journey) throws {
        do {
            CoreDataManager.shared.insertObject(journey)
            try CoreDataManager.shared.saveContext()
        } catch {
            throw UCError.databaseFailure
        }
    }

    public static func update(_ journey: Journey) throws {
        do {
            try CoreDataManager.shared.saveContext()
        } catch {
            throw UCError.databaseFailure
        }
    }

    public static func last() throws -> Journey? {
        let moc = CoreDataManager.shared.viewContext
        do {
            let request: NSFetchRequest<Journey> = Journey.fetchRequest()
            request.predicate = NSPredicate(format: "endDate == nil")
            return try moc.fetch(request).last
        } catch {
            throw error
        }
    }

}
