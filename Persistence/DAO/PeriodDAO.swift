//
//  PeriodDAO.swift
//  Ponto
//
//  Created by Marcos Kobuchi on 02/05/18.
//  Copyright © 2018 undercaffeine. All rights reserved.
//

import Foundation
import Infrastructure

public class PeriodDAO {

    public static func create(_ period: Period) throws {
        do {
            CoreDataManager.shared.insertObject(period)
            try CoreDataManager.shared.saveContext()
        } catch {
            throw UCError.databaseFailure
        }
    }

    public static func update(_ period: Period) throws {
        do {
            try CoreDataManager.shared.saveContext()
        } catch {
            throw UCError.databaseFailure
        }
    }

}
