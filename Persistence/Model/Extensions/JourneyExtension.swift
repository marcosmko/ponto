//
//  JourneyExtension.swift
//  Persistence
//
//  Created by Marcos Kobuchi on 23/05/18.
//  Copyright © 2018 undercaffeine. All rights reserved.
//

import Foundation
import CoreData
import Infrastructure

public extension Journey {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Journey> {
        return NSFetchRequest<Journey>(entityName: "Journey")
    }

    convenience public init() {
        let managedObjectContext: NSManagedObjectContext = CoreDataManager.shared.viewContext
        let entityDescription: NSEntityDescription = NSEntityDescription.entity(forEntityName: "Journey", in: managedObjectContext)!
        self.init(entity: entityDescription, insertInto: nil)
    }

}

// MARK: Generated accessors for periods
public extension Journey {

    @objc(addPeriodsObject:)
    @NSManaged public func addToPeriods(_ value: Period)

    @objc(removePeriodsObject:)
    @NSManaged public func removeFromPeriods(_ value: Period)

    @objc(addPeriods:)
    @NSManaged public func addToPeriods(_ values: NSSet)

    @objc(removePeriods:)
    @NSManaged public func removeFromPeriods(_ values: NSSet)

}
