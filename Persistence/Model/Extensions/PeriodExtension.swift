//
//  PeriodExtension.swift
//  Persistence
//
//  Created by Marcos Kobuchi on 23/05/18.
//  Copyright © 2018 undercaffeine. All rights reserved.
//

import Foundation
import CoreData
import Infrastructure

public extension Period {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Period> {
        return NSFetchRequest<Period>(entityName: "Period")
    }

    convenience public init() {
        let managedObjectContext: NSManagedObjectContext = CoreDataManager.shared.viewContext
        let entityDescription: NSEntityDescription = NSEntityDescription.entity(forEntityName: "Period", in: managedObjectContext)!
        self.init(entity: entityDescription, insertInto: nil)
    }

}
